word2vec for Debian
-------------------

This package provides an implementation of the Continuous Bag-of-Words
(CBOW) and the Skip-gram model (SG), as well as several demo scripts.

Given a text corpus, the word2vec tool learns a vector for every word
in the vocabulary using the Continuous Bag-of-Words or the Skip-Gram
neural network architectures. The user should to specify the
following:

 - desired vector dimensionality
 - the size of the context window for either the Skip-Gram or the
   Continuous Bag-of-Words model
 - training algorithm: hierarchical softmax and / or negative sampling
 - threshold for downsampling the frequent words 
 - number of threads to use
 - the format of the output word vector file (text or binary)

Usually, the other hyper-parameters such as the learning rate do not
need to be tuned for different training sets.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>, Thu, 14 Nov 2013 14:05:29 +0900
